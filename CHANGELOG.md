## Bottery Nightly

_Changes to be released_


## Bottery 0.1.1 (2018-04-28)

#### Features

- Add Middlewares to message/response process ([#155](https://github.com/rougeth/bottery/issues/155)).

#### Docs

- Add Github templates for issues and pull requests ([#151](https://github.com/rougeth/bottery/issues/151)).


## Bottery 0.1.0 (2018-04-22)

#### Features

- Telegram support with polling and webhook modes.
- Telegram widgets for replying and keyboard message.
- Facebook Messenger support.
- Create Handlers (message, startswith, regex and default).
- Create function for template rendering.
